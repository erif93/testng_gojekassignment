package testNG;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import scriptPage.Amazon_ChooseFirstHeadPhone;
import scriptPage.Amazon_DataDriven;
import scriptPage.Amazon_Login;
import scriptPage.Amazon_RemoveChart;
import scriptPage.Amazon_SearchMacbook;


@Listeners(value=customReport.class)

public class NewTest {
	
	WebDriver driver;
	Amazon_Login runnerLogin;
	Amazon_ChooseFirstHeadPhone runnerHeadPhone;
	Amazon_DataDriven datadriven;
	Amazon_SearchMacbook runnerMac;
	Amazon_RemoveChart runnerRemoveChart;
	Amazon_SearchMacbook runnerSearch;
	String AmazonMail = "assestgojek99@gmail.com";
	String AmazonPassword = "testgojek99";
	String MacbookPro = "Macbook pro";
	String userDir = System.getProperty("user.dir");
	String ChromeDriver = userDir+"\\driver77\\chromedriver.exe";
	
@BeforeTest
public void beforeTest() {
	
	System.setProperty("webdriver.chrome.driver",ChromeDriver); 
	
	driver = new ChromeDriver();
	
	driver.manage().window().maximize();

    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    driver.get("http://amazon.com");
	  }
  @Test(priority=0)
  public void testNGLoginPage() throws InterruptedException {
	  
	  runnerLogin = new Amazon_Login(driver);
	  
	  runnerLogin.clickDivSignIn();
	  
	  runnerLogin.loginAmazon(AmazonMail, AmazonPassword);
	  
	  
  }
  
  
  @Test(priority=2)
  
  public void testNGSearchMacBook() throws InterruptedException {
	  
	  runnerSearch = new Amazon_SearchMacbook(driver);
	  
	  runnerMac = new Amazon_SearchMacbook(driver);
	  
	  runnerSearch.searchItem(MacbookPro);
	  
	  runnerSearch.clickProduct2();
	  
	  runnerSearch.addQuantity2();
	  
	  runnerMac.addToCart();
	  

  }
  
  @Test(priority=1)
  
  public void testNGChooseFirstHeadPhone() throws InterruptedException {
	  
	  runnerHeadPhone = new Amazon_ChooseFirstHeadPhone(driver);
	  
	  runnerMac = new Amazon_SearchMacbook(driver);
	   
	  runnerHeadPhone.chooseElectronics();
	  
	  runnerHeadPhone.clickFirstHeadPhones();
	  
	  runnerMac.addToCart();
	  
  }
 
  @Test(priority=3)
 public void testNGRemoveHeadPhone() throws InterruptedException {
	    
	  runnerRemoveChart = new Amazon_RemoveChart(driver);
	 
	 // runnerHeadPhone.chooseElectronics();
	  
	 // runnerHeadPhone.clickFirstHeadPhones();
	  
	//  runnerMac.addToCart();
	  
	  runnerRemoveChart.removeChartFromHome();
	  
	  
  }
  
  @Test(priority=4)
  
  public void testNGRemoveMac() throws InterruptedException {
	  
	  runnerRemoveChart.reduceQty_1();
	  runnerMac.clickCheckOut();
	  
	  
  }
  
  
  @Test(priority=5)
  public void testNGParameterizeSearch() throws IOException {
	  
	  datadriven = new Amazon_DataDriven(driver);
	  
	  datadriven.searchParameterized();
	  
  }
  
  @Test(priority=6)
  public void testNGLogout() {
	  
	  runnerLogin.Logout();
	  
  }
  

  @AfterMethod
  public void afterMethod() {
  }

}
