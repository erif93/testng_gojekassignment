package scriptPage;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Amazon_RemoveChart {
	
	WebDriver driver;
	JavascriptExecutor js;
	By spanChart = By.id("nav-cart-count");
	By btnDelete = By.xpath("//input[@value = 'Delete']");
	By spanQTY = By.xpath("//span[@class = 'a-button-text a-declarative']");
	By liQTY = By.xpath("//div[@id='a-popover-4']/div/div/ul/li/a");
	
	public Amazon_RemoveChart(WebDriver driver) {
		
		
		this.driver=driver;
	}
	
	public void removeChartFromHome() throws InterruptedException {
		
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		js = (JavascriptExecutor)driver;
		
		Thread.sleep(1000);
		
		WebElement btnChart = driver.findElement(spanChart);
		
		js.executeScript("arguments[0].click();", btnChart);
		
		Thread.sleep(1000);
		
		 List<WebElement> deleteList = driver.findElements(btnDelete);
		
		 deleteList.get(1).click();
		
	}
	
public void reduceQty_1() throws InterruptedException {
	
	
		WebElement Chart = driver.findElement(spanChart);
	
		js.executeScript("arguments[0].click();", Chart);
		
		Thread.sleep(1000);
		
		List<WebElement> spanList = driver.findElements(spanQTY);
		
		spanList.get(0).click();
		
		Thread.sleep(1000);
		
		List<WebElement> liQTYList = driver.findElements(liQTY);
		
		liQTYList.get(1).click();
			
		
	}

}
