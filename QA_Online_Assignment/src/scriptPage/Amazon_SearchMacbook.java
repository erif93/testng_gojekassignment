package scriptPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class Amazon_SearchMacbook {
  
	
	
	WebDriver driver;
	WebDriver wait;
	By txtboxSearch = By.id("twotabsearchtextbox");
	By btnSearch = By.xpath("//input[@type = 'submit' and @value = 'Go']");
	By hrefProduct2= By.xpath("//span[@class = 'a-size-medium a-color-base a-text-normal']");
	By btnCart = By.id("add-to-cart-button");
	By dropdownQTY = By.id("quantity");
	By btnCheckout = By.xpath("//input[@name = 'proceedToCheckout']");
	By labelShipping = By.id("//h1[@class = 'a-spacing-base']");
	
	
		
	
	public Amazon_SearchMacbook(WebDriver driver) {
		
		this.driver=driver;
		
	}
	
	public void setItem(String item) {
		
		driver.findElement(txtboxSearch).sendKeys(item);
	}
	
	
	
	public void searchItem(String item)  {
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		this.setItem(item);
		
		driver.findElement(btnSearch).click();
	
	}
	
	public void clickProduct2() throws InterruptedException {
		
		Thread.sleep(1000);
		
		List<WebElement> elements = driver.findElements(hrefProduct2);
		
		elements.get(1).click();
		
		
	}
	
	
	public void addQuantity2() {
		
		Select dropdown = new Select(driver.findElement(dropdownQTY));
		
		dropdown.selectByIndex(1);
		
	}
	
	public void addToCart()  {
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(btnCart).click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		
			e.printStackTrace();
		}
		
		driver.get("http://amazon.com");
		
	}
	
	public void clickCheckOut() throws InterruptedException {
		
		
		Thread.sleep(2000);
		
		try {
			
		driver.findElement(btnCheckout).click();
		
		}catch (Exception e) {
			
			
		}
		if(driver.findElement(labelShipping).getText().contains("shipping")) {
			
			System.out.println("Checkout Success");
		}
		
		else {
			
			
			System.out.println("Checkout Failed");
		}

	}
	
	
	
}
