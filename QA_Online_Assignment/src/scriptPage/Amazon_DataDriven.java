package scriptPage;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
	
public class Amazon_DataDriven {

	
	WebDriver driver;
	WebDriverWait wait;
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	HSSFCell cell;
	By txtboxSearch = By.id("twotabsearchtextbox");
	By btnSearch = By.xpath("//input[@type = 'submit' and @value = 'Go']");
	
	
	
	
	public Amazon_DataDriven(WebDriver driver) {
		
		this.driver=driver;
		
		
	}
	public void searchParameterized() throws IOException {
		
				 String userDir = System.getProperty("user.dir");
				 
				 String excelFile = userDir+"\\datadriven.xls";
				 
			     File src=new File(excelFile);
			    
			     FileInputStream finput = new FileInputStream(src);
			 
			     workbook = new HSSFWorkbook(finput);
			     
			     sheet= workbook.getSheetAt(0);
			     
			     
			     for(int i=1; i<=sheet.getLastRowNum(); i++)
			     	     {
			     	         cell = sheet.getRow(i).getCell(1);
			     
			     	         driver.findElement(txtboxSearch).sendKeys(cell.getStringCellValue());
			     	         
			     	         driver.findElement(btnSearch).click();
			     	         
			     	         driver.findElement(txtboxSearch).clear();     
		
			     	     }
			     
	}
}
