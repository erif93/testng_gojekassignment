package scriptPage;

import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Amazon_ChooseFirstHeadPhone {

	WebDriverWait wait;
	WebDriver driver;
	By hrefProduct1 = By.xpath("//span[@class = 'a-size-medium a-color-base a-text-normal']");
	By btnHamburgerMenu = By.id("nav-hamburger-menu");
	By menuList = By.cssSelector("#hmenu-content > ul.hmenu.hmenu-visible > li > a");
	By hrefDepartment = By.xpath("//a[@id='nav-link-shopall']/span[2]");
	By spanHeadPhones = By.xpath("//a[@class = 'a-link-normal fsdLink fsdDeptLink' and (text() = 'Headphones' or . = 'Headphones')]");
	By divItemList = By.xpath("//div[@class = 'nav-template nav-flyout-content nav-tpl-itemList']");
	By spanElectronics = By.xpath("//span[@class = 'nav-text' and (text() = 'Electronics' or . = 'Electronics')]");
	By spanHeadPhonesCategory = By.xpath("//span[@class = 'a-color-base' and (contains(text(), 'HEADPHONES') or contains(., 'HEADPHONES'))]");
	
	
	public Amazon_ChooseFirstHeadPhone(WebDriver driver) {
		
		
		this.driver=driver;
	}
	
	
	public void chooseElectronics() throws InterruptedException {
		
		if( driver.findElements(btnHamburgerMenu).size()>0) {
		
			driver.findElement(btnHamburgerMenu).click();
		
			Thread.sleep(1000);
	
			List<WebElement> liMenu = driver.findElements(menuList);
		
			for (WebElement li : liMenu) {
					if (li.getText().equals("Electronics")) {
						li.click();
					}
				}
			
			Thread.sleep(1000);
			
			List<WebElement> liSubMenu = driver.findElements(menuList);
			
			 for (WebElement liSub : liSubMenu) {
				 
				    if(liSub.getText().equals("Headphones")) {
				    	
				    	liSub.click();
				    	
				    	 break;
				    }        
			 }
	
		}  else {
			
			
			driver.findElement(hrefDepartment).click();
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			 		if(driver.findElements(spanHeadPhones).size()> 0) {
			   
			 		driver.findElement(spanHeadPhones).click();
			   
			 		} else {
			 			
			 		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		    	
			 		driver.findElement(spanElectronics).click();
			 		
			 		driver.findElement(spanHeadPhonesCategory).click();
		    }
	    }
   }
	
	   public void clickFirstHeadPhones() throws InterruptedException {
		   
		   Thread.sleep(1000);
		   
		   List<WebElement> headPhoneList = driver.findElements(hrefProduct1);
		      
		   headPhoneList.get(0).click();
		      
		   
	   }
}
