package scriptPage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Amazon_Login {

	WebDriver driver;
	WebDriverWait wait;
	By divSignIn = By.id("nav-link-accountList");
	By spanSignIn = By.xpath("//div[@id='nav-flyout-ya-signin']/a/span");
	By txtboxEmail = By.id("ap_email");
	By txtboxPassword = By.id("ap_password");
	By btnContinue = By.id("continue");
	By btnSubmit = By.id("signInSubmit");
	By authText = By.xpath("//div[@class = 'a-row']");
	By spanSignOut = By.xpath("//a[@id='nav-item-signout']/span");
	By btnSubmitOTP = By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Enter OTP'])[1]/following::input[2]");
	
	
     public Amazon_Login(WebDriver driver) {
    	 
    	 this.driver=driver;
    	 
     }
     
     public void clickDivSignIn() {
 
    	 driver.findElement(divSignIn).click();
    	 
    	 driver.findElement(spanSignIn).click();
    	 
    	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	 
    	 
     }
     
     public void setEmail(String email)  {
    	 
    	
    	  
          driver.findElement(txtboxEmail).sendKeys(email);    	 
     }
     
     public void setPassword(String pswrd)  {
    	 
    	
    	 
    	 driver.findElement(txtboxPassword).sendKeys(pswrd);
    	 
    	 
     }
     
     
     public void loginAmazon(String email, String pswrd) throws InterruptedException {
    	 
    	 WebElement element = (new WebDriverWait(driver, 30))
    			   .until(ExpectedConditions.elementToBeClickable(txtboxEmail));
    	 
    	 driver.findElement(txtboxEmail).click();
    	 
    	 this.setEmail(email);
    	 
    	 driver.findElement(btnContinue).click();
    	 
    	 this.setPassword(pswrd);
    	 
    	 driver.findElement(btnSubmit).click();
    	 
    	 if(driver.findElement(authText).getText().contains("email")) {
    		 
    		 
     		System.out.println("Login Success"); 
     		
     		driver.findElement(btnContinue).click();
     		
     	 }  else {
     		 
     		 
     		 System.out.println("Login Failed");
     	 }
    	 
    	//input OTP manually (delay 30 s)
    	 Thread.sleep(30000);
    	 
    	 try {
    		  
    		 driver.findElement(btnSubmitOTP).click();
    	
    	 } catch(Exception e) {
    		 
    		 e.printStackTrace();
    	 	}
    	 
     }
     
     
     public void Logout() {
    	 
    	 driver.get("http://amazon.com");
    	 
    	 driver.findElement(divSignIn).click();
    	 
    	 driver.findElement(spanSignOut).click();
    	 
    	 
     }
     
     
	
}
